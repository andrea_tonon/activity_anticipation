#include <Standard.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <cmath>

using namespace std;
using namespace Eigen;



void Standard::standardizza(MatrixXd& tra)
{
  MatrixXd Rv;
  allinea(tra,Rv);
  normalizza(tra);
  ruota(tra);
}


void Standard::allinea(MatrixXd& tra,MatrixXd& Rv)
{
  int n = tra.cols();
  Vector3d v(tra.col(n-1)-tra.col(0));
  v.normalize();
  Vector3d vs(1.,1.,1.);
  vs.normalize();
  Vector3d o = vs.cross(v);
  double c = vs.dot(v);
  double s = o.norm();
  o.normalize();
  Matrix<double,3,3> O;
  O(0,0) = 0.;
  O(1,0) = o(2);
  O(2,0) = -o(1);
  O(0,1) = -o(2);
  O(1,1) = 0.;
  O(2,1) = o(0);
  O(0,2) = o(1);
  O(1,2) = -o(0);
  O(2,2) = 0.;
  Matrix<double,3,3> R = c*MatrixXd::Identity(3,3)+s*O+o*o.transpose()*(1-c);
  tra = R*tra;
  Rv = R;

 }

  void Standard::normalizza(MatrixXd& tra)
  {
    int n = tra.cols();
    for(int i=1;i<n;i++)
    {
      tra(0,i)=(tra(0,i)-tra(0,0))/(tra(0,n-1)-tra(0,0));
      tra(1,i)=(tra(1,i)-tra(1,0))/(tra(1,n-1)-tra(1,0));
      tra(2,i)=(tra(2,i)-tra(2,0))/(tra(2,n-1)-tra(2,0));
    }
    tra(0,0)=0.;
    tra(1,0)=0.;
    tra(2,0)=0.;
  }

  void Standard::ruota(MatrixXd& tra)
  {
    int n=tra.cols();
    Vector3d vect_punto(0.,0.,0.) ;
    Vector3d vs(1.,1.,1.);
    Vector3d ps(-1.,-1.,1.);
    for(int i=1;i<n-1;i++)
    {
       Vector3d temp = tra.col(0)-tra.col(i);
       double c_orto = -(temp.dot(vs))/(pow(vs.norm(),2));
       vect_punto+= tra.col(i) - (tra.col(0) - c_orto*vs);
    }
    vect_punto/=(n-2);
    double c = (ps.dot(vect_punto))/(ps.norm()*vect_punto.norm());
    vect_punto.normalize();
    vs.normalize();
    ps.normalize();
    Vector3d cr = ps.cross(vs);
    Matrix<double,3,3> Rif;
    Rif.col(0) = ps;
    Rif.col(1) = vs;
    Rif.col(2) = cr;
    vect_punto = Rif.transpose() * vect_punto;
    double a = -acos(c);
    if(vect_punto(2)>0) a*=-1.;
    double s = sin(a);
    c = cos(a);
    Matrix<double,3,3> O;
    O(0,0) = 0.;
    O(1,0) = vs(2);
    O(2,0) = -vs(1);
    O(0,1) = -vs(2);
    O(1,1) = 0.;
    O(2,1) = vs(0);
    O(0,2) = vs(1);
    O(1,2) = -vs(0);
    O(2,2) = 0.;
    Matrix<double,3,3> R = c*MatrixXd::Identity(3,3)+s*O+vs*vs.transpose()*(1-c);
    tra = R*tra;
}



