#include <eigen3/Eigen/Dense>
#include <vector>
#include <cmath>
#include <spline.h>
#include <random>
#include <Bezier.h>

using namespace std;
using namespace Eigen;

void Bezier::createQuadratic(VectorXd& curve2, VectorXd& points)
{
  int max_campioni = 50;
  int min_campioni = 15;
  int min_control_point = -3;
  int max_control_point = 3;
  // Generazione del numero di campioni e del control point
  float point = min_control_point + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX/(max_control_point - min_control_point)));
  int n = rand()%(max_campioni-min_campioni+1)+min_campioni;
  MatrixXd berz(n,3);
  VectorXd time(n);
  for(int i=0;i<n;i++) time(i) = i/(n-1.);
  Vector3i coef;
  coef << 1,2,1;
  for(int j=0;j<n;j++)
  {
    for(int i=0;i<3;i++)
    {
      berz(j,i) = coef(i) * pow(time(j),i) * pow((1.-time(j)),2.-i);
    }
  }
  VectorXd p(3);
  p << 0., point, 1.;
  // Generazione di rumore gaussiano
  const double mean = 0.0;
  const double stddev = 0.1;
  default_random_engine generator;
  normal_distribution<double> dist(mean, stddev);
  VectorXd curve(n);
  curve = berz * p;
  for (int i=0;i<n;i++)
  {
    curve(i) = curve(i) + dist(generator);
  }
  curve(0) = 0.0;
  curve(0) = 0.0;
  curve(n-1) = 1.0;
  curve(n-1) = 1.0;
  curve2 = curve;
  points = p;
}

void Bezier::createCubic(VectorXd& curve3, VectorXd& points)
{
  int max_campioni = 50;
  int min_campioni = 15;
  int min_control_point1 = -2;
  int max_control_point1 = 2;
  int min_control_point2 = 1;
  int max_control_point2 = 3;
  float point_1 = min_control_point1 + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX/(max_control_point1-min_control_point1)));
  float point_2 = min_control_point2 + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX/(max_control_point2-min_control_point2)));
  int n = rand()%(max_campioni-min_campioni+1)+min_campioni;
  MatrixXd berz(n,4);
  VectorXd time(n);
  for(int i=0;i<n;i++) time(i) = i/(n-1.);
  Vector4i coef;
  coef << 1,3,3,1;
  for(int j=0;j<n;j++)
  {
    for(int i=0;i<4;i++)
    {
      berz(j,i) = coef(i) * pow(time(j),i) * pow((1.-time(j)),3.-i);
    }
  }
  VectorXd p(4);
  p << 0., point_1, point_2, 1.;
  const double mean = 0.0;
  const double stddev = 0.1;
  default_random_engine generator;
  normal_distribution<double> dist(mean, stddev);
  VectorXd curve(n);
  curve = berz * p;
  for (int i=0;i<n;i++)
  {
    curve(i) = curve(i) + dist(generator);
  }
  curve(0) = 0.0;
  curve(n-1) = 1.0;
  curve3 = curve;
  points = p;
}

void Bezier::interpolate(int degree, VectorXd& curve, VectorXd& points, VectorXd& output){
  int n = curve.size();
  // Numero di frame visti: variabile uniforme tra 4 e i campioni della curva
  int min_frame = 4;
  int n_int = rand()%(n-min_frame+1)+min_frame;
  vector<double> X, Y;
  VectorXd times(6);
  for(int i=0;i<n_int;i++)
  {
    Y.push_back(curve(i));
    X.push_back(i/(n-1.));
  }
  double max_time = X[n_int-1];
  tk::spline s;
  s.set_points(X,Y);
  // Calcolo dei 6 punti della curva fittata, dei rispettivi tempi, velocità ed accelerazione
  int dim;
  if(degree==3) dim = 22;
  else dim = 21;
  VectorXd out(dim);
  int g = 7;
  int h = 13;
  out(0) = 0.;
  out(6) = 1.;
  out(g) = 0.;
  out(h) = 0.;
  out(19) = n_int;
  out(20) = points(1);
  if(degree==3) out(21) = points(2);
  times(0) = 0.0;
  for(int i=1;i<6;i++)
  {
    times(i) = max_time/(5.)*i;
    out(i) = s(times(i));
    out(g+i) = (out(i)-out(i-1))/(max_time/(5.));
    out(h+i) = (out(g+i)-out(g+i-1))/(max_time/(5.));
  }  
  output = out;
}


void Bezier::interpolateRegression(int n_int, int degree, MatrixXd& curve, MatrixXd& output){
  int n = curve.cols();
  vector<double> x, y, z, t;
  for(int i=0;i<n;i++)
  {
    x.push_back(curve(0,i));
    y.push_back(curve(1,i));
    z.push_back(curve(2,i));
    t.push_back(i/(n-1.));
  }
  int dim;
  if(degree==3) dim = 22;
  else dim = 21;
  MatrixXd out(dim,3);
  double max_time = t[n_int-1];
  tk::spline sx;
  sx.set_points(t,x);
  tk::spline sy;
  sy.set_points(t,y);
  tk::spline sz;
  sz.set_points(t,z);
  // Calcolo dei 6 punti della curva fittata, dei rispettivi tempi, velocità ed accelerazione
  int g = 7;
  int h = 13;
  out.row(0) << 0., 0., 0.;
  out.row(6) << 1., 1., 1.;
  out.row(g) << 0., 0., 0.;
  out.row(h) << 0., 0., 0.;
  out.row(19) << n_int, n_int, n_int;
  out.row(20) << 0., 0., 0.;
  VectorXd times(6);
  if(degree==3) out.row(21) << 0., 0., 0.;
  times(0) = 0.0;
  for(int i=1;i<6;i++)
  {
    times(i) = max_time/(5.)*i;
    out.row(i) << sx(times(i)),sy(times(i)),sz(times(i));
    out.row(g+i) = (out.row(i)-out.row(i-1))/(max_time/(5.));
    out.row(h+i) = (out.row(g+i)-out.row(g+i-1))/(max_time/(5.));
  }
  output = out;
 }


void Bezier::recreate(MatrixXd& curve, MatrixXd& points, int n_time)
{
  int n = points.cols();
  MatrixXd p = points.transpose();
  MatrixXd berz(n_time,n);
  MatrixXd cur;
  VectorXd time(n_time);
  for(int i=0;i<n_time;i++) time(i) = i/(n_time-1.);
  if(n==3)
  {
    Vector3i coef;
    coef << 1,2,1;
    for(int j=0;j<n_time;j++)
    {
      for(int i=0;i<3;i++)
      {
        berz(j,i) = coef(i) * pow(time(j),i) * pow((1.-time(j)),2.-i);
      }
    }
  }
  else{
    Vector4i coef;
    coef << 1,3,3,1;
    for(int j=0;j<n_time;j++)
    {
      for(int i=0;i<4;i++)
      {
        berz(j,i) = coef(i) * pow(time(j),i) * pow((1.-time(j)),3.-i);
      }
    }
  }
  cur = berz * p;
  curve = cur.transpose();
}



