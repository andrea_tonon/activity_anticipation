#include <Posizioni.h>
#include <iostream>
#include <fstream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <cmath>

using namespace std;
using namespace Eigen;

void Posizioni::preprocessing(MatrixXd& MT, MatrixXd& MB, MatrixXd& pc, VectorXd& tc)
{
  int n = MT.cols();
  int l = MB.rows()/3;
  MatrixXd trajs = MT - MB;
  for(int i=0;i<3*l;i+=3){
    for(int j=0;j<n;j++){
      pc(i/3,j) = sqrt(pow(trajs(i,j),2) + pow(trajs(i+1,j),2) + pow(trajs(i+2,j),2));
    }
  }
  for(int i=0;i<l;i++){
    for(int j=1;j<n;j++){
      pc(i,j) = (pc(i,j) - pc(i,0))/(pc(i,n-1) - pc(i,0));
    }
    pc(i,0) =  0.;
  }
  for(int i=0;i<n;i++){
    tc(i) = i/(n-1.);
  }
}

void Posizioni::ricostruzione(MatrixXd& MT, MatrixXd& MB, VectorXd& pc){
  int l = MB.rows();
  int n = pc.size();
  Vector3d sp(0.,0.,0.);
  Vector3d ep(1.,1.,1.);
  for(int i=0;i<l;i++){
    Vector3d temp = sp-MB.col(i);
    MT(i,0) = temp.norm();
    temp = ep-MB.col(i);
    MT(i,n-1) = temp.norm();
  }
  for(int i=0;i<l;i++){
    for(int j=0;j<n;j++){
      MT(i,j) = pc(j)*(MT(i,n-1)-MT(i,0))+MT(i,0);
    }
  }
}
