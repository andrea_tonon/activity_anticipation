#include <magic_folder/matrix_operations.h>
#include <magic_folder/interpolation.h>
#include <iostream>
#include <ros/ros.h>

#include <opencv2/ml/ml.hpp>

// GML Library
#include <gml/core/component.h>
#include <gml/train/em.h>
#include <gml/train/igmm.h>
#include <gml/train/kmeans.h>
#include <gml/regression/gaussian_regression.h>
#include <omp.h>
#include <activity_anticipation/gmm_service.h>
#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

const int NUM_COMPONENTS = 6;               
const int DIM_POINTS = 22;                  
const int TO_ESTIMATE = 2;

typedef gml::GaussianComponent<Eigen::Matrix<double, DIM_POINTS, 1> > Component;
typedef gml::GaussianMixture<Component, NUM_COMPONENTS> Mixture;

typedef Mixture::ComponentPtr ComponentPtr;
typedef Mixture::Ptr MixturePtr;

typedef Component::Point Point;
typedef Component::CovarianceMatrix CovarianceMatrix;

typedef gml::GaussianMixture<Component,NUM_COMPONENTS> DynamicMixture;
typedef std::shared_ptr<DynamicMixture> DynamicMixturePtr;

bool build_model;
std::string
path,
path_model,
train_name,
model_name;


int loadGMMModel(MixturePtr& mixture, const std::string& path, const std::string& model_name)
{
    gml::GaussianRegression<Component, NUM_COMPONENTS> gmr;

    Eigen::MatrixXd means, covs, priors;

    magic_folder::MatrixOperations::loadFromFile(path + model_name + "_mean.txt",means);
    magic_folder::MatrixOperations::loadFromFile(path + model_name + "_cov.txt",covs);
    magic_folder::MatrixOperations::loadFromFile(path + model_name + "_priors.txt",priors);

    int NUM_COMPONENTS = means.cols();
    for (int i_cmp=0; i_cmp < NUM_COMPONENTS; i_cmp++ )
    {
        Point mean = means.col(i_cmp);
        CovarianceMatrix cov = CovarianceMatrix::Identity(DIM_POINTS,DIM_POINTS);

        for (int j = 0; j < DIM_POINTS; j++)
            cov.col(j) = covs.block(j*DIM_POINTS, i_cmp, DIM_POINTS, 1);

        mixture->setComponent(i_cmp, ComponentPtr(new Component(mean, cov)));
        mixture->setWeight(i_cmp, priors(i_cmp,0));
        mixture->components(i_cmp).update();
    }

    mixture->setMinEigs(0.001f);

    gmr.setMixture(mixture);
    gmr.preProcess<Point,1>();

    return 1;
}

void trainGMM(MixturePtr mixture, const Eigen::MatrixXd& data)
{

    mixture->setMinEigs(0.0001f);


    gml::KMeans<Component, NUM_COMPONENTS> kmeans;
    kmeans.setMixture(mixture);
    kmeans.train(data,gml::KMeans<Component, NUM_COMPONENTS>::KMEANS_PPCENTERS);
    gml::EM<Component, NUM_COMPONENTS> em;


    em.setMixture(mixture);
    double llik = em.train(data);


}

void saveMeanAndCov(const MixturePtr mixture, const Eigen::MatrixXd& train_data, std::string& path, std::string& train_name)
{
    int n_c = mixture->size();
    Eigen::MatrixXd
    mean(train_data.rows(),n_c),
    temp(train_data.rows()*train_data.rows(),1),
    cov(train_data.rows()*train_data.rows(),n_c),
    priors(n_c,1);

    const auto& c_mixture = *mixture;

    for (int i=0; i<n_c;i++)
    {
        const Component& cmp = c_mixture.components(i);
        mean.col(i) = cmp.mean();
        for (int j = 0; j < train_data.rows(); j++)
            temp.block(j*train_data.rows(),0,train_data.rows(),1) = cmp.cov().col(j);
        cov.col(i) = temp;
        priors(i,0) = c_mixture.weights(i);
    }

    magic_folder::MatrixOperations::saveToFile(mean, path + train_name + "_mean.txt");
    magic_folder::MatrixOperations::saveToFile(cov,path + train_name + "_cov.txt");
    magic_folder::MatrixOperations::saveToFile(priors,path + train_name + "_priors.txt");
}

void computeRegression(const MixturePtr mixture, const Eigen::MatrixXd& test_data, std::string& path, std::string& train_name, std::vector<double>& out)
{
    gml::GaussianRegression<Component, NUM_COMPONENTS> gmr;
    gmr.setMixture(mixture);

    Eigen::MatrixXd
            estimated(test_data.rows(),test_data.cols());

    Point
            point,
            result;

    for (int i=0; i<test_data.cols(); i++)
    {
        point = test_data.col(i);
        gmr.process<Point,TO_ESTIMATE>(point,result);
        estimated.col(i) = result;
        out.at(i) = estimated(20,i);
        out.at(3+i) = estimated(21,i);
    }
    //magic_folder::MatrixOperations::saveToFile(estimated,path + train_name + "_estimated.txt");
}


bool execute(activity_anticipation::gmm_service::Request &req, activity_anticipation::gmm_service::Response &res){
  DynamicMixturePtr mixture(new DynamicMixture);
  if(build_model)
  {
    Eigen::MatrixXd train_data;

    // LOADING TRAIN DATA
    if(!magic_folder::MatrixOperations::loadFromFile(path + train_name + ".txt", train_data))
    {
        std::cout << "Could not read data " << std::endl;
        return 0;
    }

    std::cout << "Training the GMM..." << std::endl;
    clock_t global_time = clock();
    trainGMM(mixture,train_data);
    std::cout << "Time to run the training was " << (float)(clock() - global_time)/CLOCKS_PER_MS << " ms. \n" << std::endl;
    saveMeanAndCov(mixture, train_data, path, train_name);
  }

  else
  {
    std::cout << "Loading the GMM..." << std::endl;
    loadGMMModel(mixture, path_model, model_name);
    std::cout << "Loading done" << std::endl;
  }

  Eigen::MatrixXd test_data(22,3);
  for(int i=0;i<req.test.size();i++){
    test_data(i%22,i/22) = req.test.at(i);
  }
  std::vector<double> out(6);

  clock_t global_time = clock();
  computeRegression(mixture,test_data,path,train_name,out);
  std::cout << "Time to run the regression was " << (float)(clock() - global_time)/CLOCKS_PER_MS << " ms. \n" << std::endl;

  res.points = out;
  std::cout << "Executed..." << std::endl;
  return true;

}








// =============================================================================================================================

int main(int argc, char *argv[])
{

  
  
  ros::init(argc, argv, "activity_anticipation_cubic_regression");
  ros::NodeHandle n;

  ROS_INFO("Loading parameters from yaml files...");
  n.param(ros::this_node::getName() + "/build_model",     build_model,    true);
  n.param(ros::this_node::getName() + "/path",            path,           std::string("/home/andrea/workspace/ros/catkin/src/activity_anticipation/Model/Cubic/"));
  n.param(ros::this_node::getName() + "/path_model",      path_model,     std::string("/home/andrea/workspace/ros/catkin/src/activity_anticipation/Model/Cubic/"));
  n.param(ros::this_node::getName() + "/train_name",      train_name,     std::string("train_3"));
  n.param(ros::this_node::getName() + "/model_name",      model_name,     std::string("train_3"));

  ros::ServiceServer service = n.advertiseService("GMR_Cubic", execute);
  ros::spin();

  return 0;
}


