#include <Ricostruisci.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <cmath>
#include <Standard.h>

using namespace std;
using namespace Eigen;


void Ricostruisci::ricostruisci(MatrixXd& tra_l, MatrixXd& bez)
{
  MatrixXd Rv;
  MatrixXd P(3,2);
  P.col(0) = tra_l.col(0);
  P.col(1) = tra_l.col(tra_l.cols()-1);
  ruota(tra_l,Rv,bez);
  P = Rv * P;
  normalizza(P,bez);
  bez = Rv.transpose() * bez;
}


void Ricostruisci::ruota(MatrixXd& tra_l, MatrixXd& Rv, MatrixXd& bez)
{
  Standard st;
  st.allinea(tra_l,Rv);
  st.normalizza(tra_l);
  int n=tra_l.cols();
  Vector3d vect_punto(0.,0.,0.) ;
  Vector3d vs(1.,1.,1.);
  Vector3d ps(-1.,-1.,1.);
  for(int i=1;i<n-1;i++)
  {
     Vector3d temp = tra_l.col(0)-tra_l.col(i);
     double c_orto = -(temp.dot(vs))/(pow(vs.norm(),2));
     vect_punto+= tra_l.col(i) - (tra_l.col(0) - c_orto*vs);
  }
  vect_punto/=(n-2);
  double c = (ps.dot(vect_punto))/(ps.norm()*vect_punto.norm());
  vect_punto.normalize();
  vs.normalize();
  ps.normalize();
  Vector3d cr = ps.cross(vs);
  Matrix<double,3,3> Rif;
  Rif.col(0) = ps;
  Rif.col(1) = vs;
  Rif.col(2) = cr;
  vect_punto = Rif.transpose() * vect_punto;
  double a = acos(c);
  if(vect_punto(2)>0) a*=-1.;
  double s = sin(a);
  c = cos(a);
  Matrix<double,3,3> O;
  O(0,0) = 0.;
  O(1,0) = vs(2);
  O(2,0) = -vs(1);
  O(0,1) = -vs(2);
  O(1,1) = 0.;
  O(2,1) = vs(0);
  O(0,2) = vs(1);
  O(1,2) = -vs(0);
  O(2,2) = 0.;
  Matrix<double,3,3> R = c*MatrixXd::Identity(3,3)+s*O+vs*vs.transpose()*(1-c);
  bez = R*bez;
 }

void Ricostruisci::normalizza(MatrixXd& P, MatrixXd& bez)
{
    int m = bez.cols();
    for(int i=1;i<m-1;i++)
    {
      bez(0,i)=bez(0,i)*(P(0,1)-P(0,0)) + P(0,0);
      bez(1,i)=bez(1,i)*(P(1,1)-P(1,0)) + P(1,0);
      bez(2,i)=bez(2,i)*(P(2,1)-P(2,0)) + P(2,0);
    }
    bez(0,0)=P(0,0);
    bez(1,0)=P(1,0);
    bez(2,0)=P(2,0);
    bez(0,m-1)=P(0,1);
    bez(1,m-1)=P(1,1);
    bez(2,m-1)=P(2,1);
}
