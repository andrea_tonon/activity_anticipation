#include <iostream>
#include <fstream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <Bezier.h>
#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

using namespace std;
using namespace Eigen;

const int curve_number = 100000;
const int degree = 3;
const string path = "../Model/";
const string file = "train";

int main(int argc, char *argv[])
{
  Bezier bg;
  int dim;
  if(degree == 2) dim = 21;
  if(degree == 3) dim = 22; 
  MatrixXd buffer(dim,curve_number);
  VectorXd output;
  VectorXd curve;
  VectorXd points;
  for(int i=0;i<curve_number;i++)
  {
    if(degree == 2)
    {
      bg.createQuadratic(curve,points);
    }
    if(degree == 3)
    {
      bg.createCubic(curve,points);
    }
    bg.interpolate(degree,curve,points,output);
    buffer.col(i) = output;
  }
  ofstream out;
  out.open ((path + file + "_" + to_string(degree) + ".txt").c_str());
  out << buffer;
  out.close();
  return 0;
}
