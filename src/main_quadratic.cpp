#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <Standard.h>
#include <Ricostruisci.h>
#include <Bezier.h>
#include <activity_anticipation/gmm_service.h>
#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

using namespace std;
using namespace Eigen;


void readTraj(string f, MatrixXd& traj, int n){
  fstream in(f);
  string line;
  vector<double> buffer;
  int n_tra = 0;
  while (getline(in, line))
  {
      double value;
      stringstream ss(line);
      while (ss >> value)
      {	
	  n_tra++;
          buffer.push_back(value);
      }
  }
  n_tra/=3;
  if(n_tra==n){
    MatrixXd tra(3,n_tra);
    int k=0;
    for(int i=0;i<3;i++){
      for(int j=0;j<n_tra;j++){
        tra(i,j) = buffer[k++];
      }
     }
     traj = tra;	
  }
  else{
    MatrixXd tra(3,n + 1);
    for(int i=0;i<3;i++){
      for(int j=0;j<n;j++){
        tra(i,j) = buffer[j + n_tra*i];
      }
    }
    tra(0,n) = buffer[n_tra-1];
    tra(1,n) = buffer[2*n_tra-1];
    tra(2,n) = buffer[3*n_tra-1];
    traj = tra;
  }
  
}

int main(int argc, char *argv[])
{

  ros::init(argc, argv, "main_quadratic");
  ros::NodeHandle nh;
  string path, name;
  int n,n_c;
  ROS_INFO("Loading parameters from yaml files...");
  nh.param(ros::this_node::getName() + "/path",       path,    std::string("/home/andrea/Scrivania/Robotica_project/Trajectories/bug/"));
  nh.param(ros::this_node::getName() + "/name",       name,    std::string("_traj3"));
  nh.param(ros::this_node::getName() + "/n",          n,       24);
  nh.param(ros::this_node::getName() + "/n_inter",    n_c,     10);

  ros::ServiceClient client = nh.serviceClient<activity_anticipation::gmm_service>("GMR_Quadratic");
  MatrixXd tra;
  MatrixXd tra_l;
  MatrixXd out;
  ros::WallTime start_, end_;
  start_ = ros::WallTime::now();
  readTraj((path + name + ".txt").c_str(),tra,n);
  tra_l = tra;
  Standard st;
  clock_t global_time = clock();
  st.standardizza(tra);
  cout << "Time to run standardizza was " << (float)(clock() - global_time)/CLOCKS_PER_MS << " ms. \n" << endl;
  Bezier bg;
  global_time = clock();
  bg.interpolateRegression(n_c,2,tra,out);
  cout << "Time to run interpolation was " << (float)(clock() - global_time)/CLOCKS_PER_MS << " ms. \n" << endl;
  vector<double> dat(63);
  for(int i=0;i<63;i++) dat.at(i) = out(i%21,i/21);
  activity_anticipation::gmm_service srv;
  srv.request.test = dat;
  if (client.call(srv)){
    MatrixXd points(3,3);
    vector<double> p = srv.response.points;
    points.col(0) << 0.,0.,0.;
    points.col(1) << p.at(0), p.at(1), p.at(2);
    points.col(2) << 1.,1.,1.;
    Bezier bg;
    MatrixXd curve;
    Ricostruisci r;
    global_time = clock();
    r.ricostruisci(tra_l,points);
    bg.recreate(curve, points, n);
    cout << "Time to run recreate was " << (float)(clock() - global_time)/CLOCKS_PER_MS << " ms. \n" << endl;
    ofstream fout;
    fout.open ((path + name + "_Quadratic_Predict.txt").c_str());
    fout << curve;
    fout.close();
    end_ = ros::WallTime::now();
    double execution_time = (end_ - start_).toNSec() * 1e-6;
    cout << "Exectution time (ms): " << execution_time << endl;
  }
  else{
    ROS_ERROR("Failed to call service GMR_Cubic");
    return 1;
  }
  return 0;
}
