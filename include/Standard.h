#include <ctime>
#include <iostream>
#include <math.h>
#include <eigen3/Eigen/Dense>



#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

class Standard
{
public:

    /**
     * @brief FaceExtractor create an object of the class FaceExtractor, this class can be used to extract the face of the person
     *          from a depth image and the corresponding rgb image.
     */

    /**
     * @brief FaceExtractor create an object of the class FaceExtractor, this class can be used to extract the face of the person
     *          from a depth image and the corresponding rgb image.
     * @param input_depth the input depth where the face will be extracted
     * @param input_rgb the input rgb image of which to extract the face
     */


    /**
     * @brief setInputImages set the input images from where to extract the face
     * @param input_depth
     * @param input_rgb
     */
    void standardizza(Eigen::MatrixXd&);

    void allinea(Eigen::MatrixXd&, Eigen::MatrixXd&);

    void normalizza(Eigen::MatrixXd&);

    void ruota(Eigen::MatrixXd&);

    /**
     * @brief getPersonCloud get the cloud of the person
     * @return the cloud with only the person
     */
   // pcl::PointCloud<pcl::PointXYZ>::Ptr getPersonCloud();
};
