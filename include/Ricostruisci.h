#include <ctime>
#include <iostream>
#include <math.h>
#include <eigen3/Eigen/Dense>



#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

class Ricostruisci
{
public:

    /**
     * @brief ricostruisci compute the bezier curve without standardization
     * @param tra_l the input real trajectory to estimate the rotation angle
     * @param bez the entire bezier curve or the control points matrix
     */
    void ricostruisci(Eigen::MatrixXd& tra_l, Eigen::MatrixXd& bez);

private:
   
    /**
     * @brief normalizza normalize the bezier curve  
     * @param P start point and end point of the real curve
     * @param bez the bezier curve to normalize
     */
    void normalizza(Eigen::MatrixXd& P, Eigen::MatrixXd& bez);
    
    /**
     * @brief ruota rotate the bezier curve  
     * @param tra_l the real trajectory to estimate the rotation angle
     * @param Rv the output rotation matrix 
     * @param bez the bezier curve to rotate
     */
    void ruota(Eigen::MatrixXd& tra_l, Eigen::MatrixXd& Rv, Eigen::MatrixXd& bez);
};
