#include <ctime>
#include <iostream>
#include <math.h>
#include <eigen3/Eigen/Dense>

class Bezier
{
public:
    
    /**
     * @brief createQuadratic generate a quadratic bezier curve for 1 variable
     * @param curve the output curve generated 
     * @param points the output 3 1d control points 
     */
    void createQuadratic(Eigen::VectorXd& curve, Eigen::VectorXd& points);
    
    /**
     * @brief createCubic generate a cubic bezier curve for 1 variable
     * @param curve the output curve generated 
     * @param points the output 4 1d control points 
     */
    void createCubic(Eigen::VectorXd& curve, Eigen::VectorXd& points);
    
    /**
     * @brief interpolate interpolate the bezier curve and compute the freatures vector for GMM model
     * @param degree the degree of the input curve
     * @param curve the input bezier curve of 1 variable  
     * @param points the 1d control points
     * @param output the output features vector for GMM model
     */
    void interpolate(int degree, Eigen::VectorXd& curve, Eigen::VectorXd& points, Eigen::VectorXd& output);
    
    /**
     * @brief interpolateRegression interpolate the bezier curve and compute the freatures vector for GMR
     * @param n_int number of points for the interpolation
     * @param degree the degree of the input curve
     * @param curve the input bezier curve of 3 variables  
     * @param output the output features vector for the GMR
     */
    void interpolateRegression(int n_int, int degree, Eigen::MatrixXd& curve, Eigen::MatrixXd& output);
    
    /**
     * @brief recreate recreate a bezier curve of 3 variables from the control points
     * @param curve the output curve recreated 
     * @param points the input 3d control points
     * @param n_time the number of time istants for the curve 
     */
    void recreate(Eigen::MatrixXd& curve, Eigen::MatrixXd& points, int n_time); 
};
