# activity_anticipation

## Funzionamento:
* roslaunch activity_anticipation gmm_regression_quadratic.launch : per l'esecuzione della regression con modello con curve di B�zier quadratiche.
* roslaunch activity_anticipation gmm_regression_cubic.launch : per l'esecuzione della regression con modello con curve di B�zier cubiche.
* Le impostazioni per l'esecuzione sono da impostare sui rispettivi file all'interno della cartella config.
* All'interno della cartella bin c'� un eseguibile che permette di generare i file di training, esecuzione ./createBezier
* Due file di training, uno per ogni modello, sono gi� diponibili nella cartella Model 

